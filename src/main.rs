mod editor;
use editor::Editor;

// There is nothing in the main file because I wanted to keep things simple and easy to use
// Let there be no errors
// sus

fn main() {
    Editor::default()
        .run();
}
